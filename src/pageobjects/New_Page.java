package pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class New_Page {

		private static WebElement element = null;
	
	public static WebElement Title(WebDriver driver) {
	element = driver.findElement(By.id("content-title"));
	return element;
	}
	
	public static WebElement Publish_Button(WebDriver driver) {
	element = driver.findElement(By.id("rte-button-publish"));
	return element;
	}
	
	public static WebElement Menu_Link(WebDriver driver) {
	element = driver.findElement(By.cssSelector("#action-menu-link > Span > span"));
	return element;
	}
	
	public static WebElement Permissions(WebDriver driver) {
	element = driver.findElement(By.cssSelector("#action-page-permissions-link > span"));
	return element;
	}
	
	public static WebElement Viewing_Radio(WebDriver driver) {
	element = driver.findElement(By.id("restrictViewRadio"));
	return element;
	}
	
	public static WebElement Permissions_User(WebDriver driver) {
	element = driver.findElement(By.linkText("Me"));
	return element;
	}
	
	//Focus Issues resolve
	public static WebElement Return_Focus(WebDriver driver) {
	element = driver.findElement(By.id("page-permissions-table"));
	return element;
	}
	
	public static WebElement Permissions_Update(WebDriver driver) {
	element =  driver.findElement(By.cssSelector("button.button-panel-button.permissions-update-button"));
	return element;
	}
}