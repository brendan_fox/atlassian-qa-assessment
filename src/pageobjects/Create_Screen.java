package pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Create_Screen {

		private static WebElement element = null;
		
	public static WebElement page_type(WebDriver driver){
	element = driver.findElement(By.cssSelector("div.template-description"));
	return element;
	}
	
	public static WebElement Create_Button(WebDriver driver){
	element = driver.findElement(By.xpath("//div[@id='create-dialog']/div/div[2]/button"));
	return element;
	}
}
