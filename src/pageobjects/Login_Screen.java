package pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login_Screen {

		private static WebElement element = null;
		
	public static WebElement Username(WebDriver driver) {
	element = driver.findElement(By.id("username"));
	return element;
	}
	public static WebElement Password(WebDriver driver) {
	element = driver.findElement(By.id("password"));
	return element;
	}

	public static WebElement Login_Button(WebDriver driver) {
	element = driver.findElement(By.id("login"));
	return element;
	}
}