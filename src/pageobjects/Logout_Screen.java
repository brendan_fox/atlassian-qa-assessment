package pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Logout_Screen {
	
		private static WebElement element = null;
		
	public static WebElement Logout_Button(WebDriver driver) {
	element = driver.findElement(By.id("logout"));
	return element;
	}
}
