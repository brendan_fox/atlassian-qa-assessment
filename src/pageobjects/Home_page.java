package pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

//Import Var Class
import pageobjects.Var;

	public class Home_page {

		private static WebElement element = null;
	
	public static WebElement Create(WebDriver driver) {
	element = driver.findElement(By.id("create-page-button"));
	return element;
	}

	public static WebElement User_Menu(WebDriver driver) {
	element = driver.findElement(By.id("user-menu-link"));
	return element;
	}	

	public static WebElement Logout(WebDriver driver) {
	element = driver.findElement(By.id("logout-link"));
	return element;
	}

	public static WebElement Page(WebDriver driver) {
	element = driver.findElement(By.linkText(Var.title2));
	return element;
	}

}