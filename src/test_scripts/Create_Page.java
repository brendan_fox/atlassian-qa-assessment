package test_scripts;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

// Import pageobjects
import pageobjects.Create_Screen;
import pageobjects.Home_page;
import pageobjects.Login_Screen;
import pageobjects.New_Page;
import pageobjects.Logout_Screen;
import pageobjects.Var;

public class Create_Page {
	
		private static WebDriver driver = null;
		
	public static void main(String[] args) {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
		// Type in Web Address
		driver.get(Var.URL);
		
			
		// Enter Username, Password and Login
		Login_Screen.Username(driver).sendKeys(Var.Username1);
		Login_Screen.Password(driver).sendKeys(Var.Password1);
		Login_Screen.Login_Button(driver).click();
		
		//Click on Create Button
		Home_page.Create(driver).click();
		
		//Select page type and create
		Create_Screen.page_type(driver).click();
		Create_Screen.Create_Button(driver).click();
		
		//Provide Titile and Publish
		New_Page.Title(driver).sendKeys(Var.title1);
		New_Page.Publish_Button(driver).click();
		
		//Log out
		Home_page.User_Menu(driver).click();
		Home_page.Logout(driver).click();
		Logout_Screen.Logout_Button(driver).click();
		
		driver.close();
		
		}
}
	


	