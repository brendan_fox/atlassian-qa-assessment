package test_scripts;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


// Import pageobjects
import pageobjects.Home_page;
import pageobjects.Login_Screen;
import pageobjects.Logout_Screen;
import pageobjects.New_Page;
import pageobjects.Var;

public class Page_Restrictions {

		private static WebDriver driver = null;
	
	public static void main (String[] args) {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Type in Web Address
		driver.get(Var.URL);
					
		//Enter Username, Password and Login
		Login_Screen.Username(driver).sendKeys(Var.Username1);
		Login_Screen.Password(driver).sendKeys(Var.Password1);
		Login_Screen.Login_Button(driver).click();
		
		//Select page
		Home_page.Page(driver).click();
		
		//Click on menu and Select Page Permissions
		New_Page.Menu_Link(driver).click();
		New_Page.Permissions(driver).click();
		New_Page.Viewing_Radio(driver).click();
		New_Page.Permissions_User(driver).click();
		New_Page.Return_Focus(driver).click();
		New_Page.Permissions_Update(driver).click();
		
		Home_page.User_Menu(driver).click();
		Home_page.Logout(driver).click();
		Logout_Screen.Logout_Button(driver).click();
		
		driver.close();
		
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Type in Web Address
		driver.get(Var.URL);
		
		//Enter Username, Password and Login
		Login_Screen.Username(driver).sendKeys(Var.Username2);
		Login_Screen.Password(driver).sendKeys(Var.Password2);
		Login_Screen.Login_Button(driver).click();
		
		driver.close();
		
	}
}
