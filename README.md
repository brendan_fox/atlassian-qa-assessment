To Whom it May Concern,

Please find within this Repo 2 Selenium Test Scripts for Confluence Cloud.
These Scripts Test the following :-

* User can Create a new Page
* User can Set Permissions for an existing page.

Pre-requisites:
* Firefox to be installed on the Test PC
* Test PC is able to run Selenium Java Scripts

The complete this, the user will need to create 2 users in Confluence Cloud.
The Usernames and Password need to be defined in the pageobjects/Var Class under Username1 and Username2
The Webaddress needs to be defined in pageobjects/Var Class

Test Scripts are located under test_scripts
These are:-
* Create_Page
* Page_Restrictions

The Scripts will perform the following:-
Cretae Page:
1. Open Firefox
2. Navigate to defined WebAddress
3. Login as User 1
4. Create Page a new blank page with title "Test Page 1" (name of page can be changed in the Var Class)

Page Restrictions:
1. Open Firefox
2. Navigate to defined WebAddress
3. Login as User 1
4. Find defined link (based in text) and open page
5. Set Permissions as User can only view page
6. Log out and Close Browser
7. Open Firefox
8. Login as User 2
9. Browser is not closed, user to check page is not displayed.

Assumptions:-
Body text is not required for the test
Log files are not required
Web Address testing for Page Restriction is not required.